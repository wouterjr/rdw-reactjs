Simpel ReactJS script to fetch car data from the RDW database through API.<br />
NOTE: input field does create uppercase, but does not strip "-", you can add it easily yourself

Check components for logic.

## Available Scripts

In the project directory, you can run:

### `npm install Axios`

I used Axios to fetch the RDW API from https://opendata.rdw.nl/
I fetch only one license plate using: https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=XXXXXX`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.