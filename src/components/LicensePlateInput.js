import React, {Component} from 'react';

class LicensePlateInput extends Component {

    getPlate(e) {
        this.props.updatePlate(e.target.value);
    }

    render() {
        return (
            <label>
                <input type="text" onChange={(e) => this.getPlate(e)} placeholder="Voer kenteken in"/>
            </label>
        );
    }
}

export default LicensePlateInput;
