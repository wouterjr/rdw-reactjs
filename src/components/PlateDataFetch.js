import React, {Component} from 'react';
import axios from 'axios';

export default class PlateData extends Component {

    state = {
        car: null
    };

    constructor(props) {
        super(props);
        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this.update();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.update();
    }

    update() {
        axios.get(`https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=` + this.props.plate)
            .then(res => {
                const cars = res.data;
                if (cars.length > 0) {
                    this.setState({car: cars[0]});
                }
            });
    }

    render() {
        const {car} = this.state;

        if (car === null) {
            return <></>;
        }

        return (
            <div className="carData">
                <h2>Informatie Auto</h2>
                <strong>Merk: </strong>{car.merk}<br/>
                <strong>Model: </strong>{car.handelsbenaming}<br/>
                <strong>Soort: </strong>{car.voertuigsoort}<br/>
                <strong>Type: </strong>{car.inrichting}<br/>
                <strong>Variant: </strong>{car.variant}<br/>
                <strong>Uitvoering: </strong>{car.uitvoering}<br/>
                <strong>Tenaamstelling: </strong>{car.datum_tenaamstelling}<br/>
                <strong>Kleur: </strong>{car.eerste_kleur}<br/>
                <strong>Aantal Deuren: </strong>{car.aantal_deuren}<br/>
                <strong>Handelsbenaming: </strong>{car.handelsbenaming}<br/>
                <strong>Vervaldatum APK: </strong>{car.vervaldatum_apk}<br/>
                <strong>Lengte: </strong>{car.lengte}<br/>
                <strong>Breedte: </strong>{car.breedte}<br/>
            </div>
        );
    }

}
