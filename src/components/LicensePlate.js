import LicensePlateInput from './LicensePlateInput';
import React, {Component} from 'react';
import PlateData from './PlateDataFetch';

class LicensePlate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            licenseplate: 'licenseplate'
        };
        this.updatePlate = this.updatePlate.bind(this);
    }

    updatePlate(licenseplate) {
        this.setState({
            licenseplate: licenseplate.toUpperCase()
        });
    }

    render() {
        return (
            <div>
                <div className='Header'>
                    <h1>RDW kenteken fetch</h1>
                    <LicensePlateInput parentState={this.state} updatePlate={this.updatePlate}/>
                    <p><strong>Kentekens om te testen: </strong>ST407R, PV559K, 39BPTL, 5VXV90, 83PLH8, RR603Z, H690BV, 2XXZ15, 96NKZF, 85RZZ3, 2TJH99*<br/>
                    <em>* Of willekeurige kentekens.</em></p>
                </div>
                <div>
                    <PlateData plate={this.state.licenseplate}/>
                </div>
                <h3>Creator: Wouter Meeuwisse</h3>
            </div>
        );
    }
}

export default LicensePlate;
