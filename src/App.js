import React, {Component} from 'react';
import LicensePlate from './components/LicensePlate';
import './App.css';

class App extends Component {

    render() {
        return (
            <div>
                <LicensePlate/>
            </div>
        );
    }
}

export default App;
